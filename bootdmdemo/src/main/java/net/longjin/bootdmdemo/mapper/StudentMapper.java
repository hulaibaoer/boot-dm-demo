package net.longjin.bootdmdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.longjin.bootdmdemo.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author ：HuChao
 * @date ：Created in 2021/1/5 14:36
 * @description：
 * @modified By：
 */
@Component
@Mapper
public interface StudentMapper extends BaseMapper<Student> {
}

package net.longjin.bootdmdemo.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author ：HuChao
 * @date ：Created in 2020/12/23 15:53
 * @description：分页查询参数
 * @modified By：
 */
@Data
public class PageQueryCondition {
    /**
     * 页数
     */
    @NotNull(message = "pageNum不能为空")
    @Min(value = 1,message = "pageNum不能小于1")
    private Integer pageNum;

    /**
     * 每页显示记录数
     */
    @NotNull(message = "pageSize不能为空")
    @Min(value = 1,message = "pageSize不能小于1")
    private Integer pageSize;
}

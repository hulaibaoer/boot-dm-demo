package net.longjin.bootdmdemo.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ：HuChao
 * @date ：Created in 2021/1/5 15:31
 * @description：
 * @modified By：
 */
@Data
public class StudentPageDto extends PageQueryCondition implements Serializable {
    private static final long serialVersionUID = 7866367919457142835L;

    /**
     * 名称
     */
    private String name;

}

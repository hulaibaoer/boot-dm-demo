package net.longjin.bootdmdemo.dto;

import lombok.Data;
import net.longjin.bootdmdemo.validated.Insert;
import net.longjin.bootdmdemo.validated.Update;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ：HuChao
 * @date ：Created in 2021/1/5 14:38
 * @description：
 * @modified By：
 */
@Data
public class StudentDto implements Serializable {

    private static final long serialVersionUID = 7997877494252253114L;

    @Null(message = "新增操作id必须为空",groups = {Insert.class})
    @NotBlank(message = "更新操作id不能为空",groups = {Update.class})
    private String id;
    /**
     * 名称
     */
    @NotBlank(message = "姓名不能为空")
    private String name;
    /**
     * 年龄
     */
    @Min(value = 7,message = "年龄最小为7岁")
    @Max(value = 30,message = "年龄最大为30岁")
    private Integer age;
    /**
     * 邮箱
     */
    @Email(message = "邮箱格式错误")
    private String email;
    /**
     * 学号
     */
    @NotBlank(message = "学号不能为空")
    private String stuId;


}

package net.longjin.bootdmdemo.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * (Student)实体类
 *
 * @author makejava
 * @since 2021-01-05 14:33:08
 */
@Data
public class Student implements Serializable {
    private static final long serialVersionUID = 780682875178597048L;

    /**
     * 主键id
     */
    private String id;

    /**
     * 名称
     */
    private String name;
    /**
    * 年龄
    */
    private Integer age;
    /**
    * 邮箱
    */
    private String email;
    /**
    * 学号
    */
    private String stuId;
    /**
    * 创建时间
    */
    private LocalDateTime createTime;
    /**
    * 修改时间
    */
    private LocalDateTime updateTime;

}
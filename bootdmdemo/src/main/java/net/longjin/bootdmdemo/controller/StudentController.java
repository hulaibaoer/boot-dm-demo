package net.longjin.bootdmdemo.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.longjin.bootdmdemo.core.BizException;
import net.longjin.bootdmdemo.core.ResBizCode;
import net.longjin.bootdmdemo.core.ResResult;
import net.longjin.bootdmdemo.dto.StudentDto;
import net.longjin.bootdmdemo.dto.StudentPageDto;
import net.longjin.bootdmdemo.entity.Student;
import net.longjin.bootdmdemo.service.StudentService;
import net.longjin.bootdmdemo.utils.DateUtil;
import net.longjin.bootdmdemo.utils.IdUtil;
import net.longjin.bootdmdemo.validated.Insert;
import net.longjin.bootdmdemo.validated.Update;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ：HuChao
 * @date ：Created in 2021/1/5 14:34
 * @description：StudentController
 * @modified By：
 */
@RestController
@RequestMapping("/demo/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @PostMapping
    public ResResult add(@RequestBody @Validated({Insert.class}) StudentDto studentDto){
        Student student = new Student();
        BeanUtils.copyProperties(studentDto,student);
        student.setId(IdUtil.uid());
        student.setCreateTime(DateUtil.now());
        student.setUpdateTime(DateUtil.now());
        studentService.save(student);
        return ResResult.success(student);
    }

    @PutMapping
    public ResResult update(@RequestBody @Validated({Update.class}) StudentDto studentDto){
        Student student = studentService.getById(studentDto.getId());
        if (ObjectUtils.isEmpty(student)){
            throw new BizException(ResBizCode.OBJ_NOT_EXIST);
        }
        BeanUtils.copyProperties(studentDto,student);
        student.setUpdateTime(DateUtil.now());
        studentService.updateById(student);
        return ResResult.success(student);
    }

    @GetMapping("/{id}")
    public ResResult query(@PathVariable("id") String id){
        Student student = studentService.getById(id);
        return ResResult.success(student);
    }

    @GetMapping
    public ResResult list(){
        List<Student> students = studentService.list();
        return ResResult.success(students);
    }

    @PostMapping("/page")
    public ResResult page(@RequestBody StudentPageDto pageDto){
        int pageNum = pageDto.getPageNum();
        int pageSize = pageDto.getPageSize();
        Page<Student> page = new Page<>(pageNum,pageSize);
        QueryWrapper<Student> studentQueryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(pageDto.getName())){
            studentQueryWrapper.lambda().like(Student::getName,pageDto.getName());
        }
        Page<Student> pageData = studentService.page(page,studentQueryWrapper);
        return ResResult.success(pageData);
    }
}

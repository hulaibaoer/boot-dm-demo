package net.longjin.bootdmdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootDmDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootDmDemoApplication.class, args);
    }

}

package net.longjin.bootdmdemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import net.longjin.bootdmdemo.entity.Student;

/**
 * @author ：HuChao
 * @date ：Created in 2021/1/5 14:35
 * @description：
 * @modified By：
 */
public interface StudentService extends IService<Student> {
}

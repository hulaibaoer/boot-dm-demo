package net.longjin.bootdmdemo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.longjin.bootdmdemo.entity.Student;
import net.longjin.bootdmdemo.mapper.StudentMapper;
import net.longjin.bootdmdemo.service.StudentService;
import org.springframework.stereotype.Service;

/**
 * @author ：HuChao
 * @date ：Created in 2021/1/5 14:37
 * @description：
 * @modified By：
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {
}

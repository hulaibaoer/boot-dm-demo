package net.longjin.bootdmdemo.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ：HuChao
 * @date ：Created in 2021/1/4 11:45
 * @description：
 * @modified By：
 */
public class DateUtil {
    /**
     * 显示年月日时分秒，例如 2015-08-11 09:51:53.
     */
    public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /**
     * 仅显示年月日，例如 2015-08-11.
     */
    public static final String DATE_PATTERN = "yyyy-MM-dd";

    /**
     * 仅显示时分秒，例如 09:51:53.
     */
    public static final String TIME_PATTERN = "HH:mm:ss";

    /**
     * 显示年月日时分秒(无符号)，例如 20150811095153.
     */
    public static final String UNSIGNED_DATETIME_PATTERN = "yyyyMMddHHmmss";

    /**
     * 仅显示年月日(无符号)，例如 20150811.
     */
    public static final String UNSIGNED_DATE_PATTERN = "yyyyMMdd";

    public static final Map<String, DateTimeFormatter> DATE_TIME_FORMATTER_MAP =
            new ConcurrentHashMap<String, DateTimeFormatter>(){
                {
                    put(DATETIME_PATTERN,DateTimeFormatter.ofPattern(DATETIME_PATTERN));
                    put(DATE_PATTERN,DateTimeFormatter.ofPattern(DATE_PATTERN));
                    put(TIME_PATTERN,DateTimeFormatter.ofPattern(TIME_PATTERN));
                    put(UNSIGNED_DATETIME_PATTERN,DateTimeFormatter.ofPattern(UNSIGNED_DATETIME_PATTERN));
                    put(UNSIGNED_DATE_PATTERN,DateTimeFormatter.ofPattern(UNSIGNED_DATE_PATTERN));
                }
            };

    public static DateTimeFormatter getDateTimeFormatter(String format){
        if (DATE_TIME_FORMATTER_MAP.containsKey(format)){
            return DATE_TIME_FORMATTER_MAP.get(format);
        }else {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            DATE_TIME_FORMATTER_MAP.put(format, formatter);
            return formatter;
        }
    }

    /**
     * 获取当前日期时间
     *
     * @return
     */
    public static LocalDateTime now(){
        return LocalDateTime.now();
    }



}

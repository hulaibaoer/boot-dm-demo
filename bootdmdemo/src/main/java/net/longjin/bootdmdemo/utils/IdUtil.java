package net.longjin.bootdmdemo.utils;

import java.util.UUID;

/**
 * @author ：HuChao
 * @date ：Created in 2020/9/30 10:47
 * @description：id生成工具类
 * @modified By：
 */
public class IdUtil {
    public static final long workId = 1L;

    /**
     * 生成全局唯一自增序列id
     *
     * @return
     */
    public static long generateId() {
        return SnowflakeIdWorker.getInstance(workId).nextId();
    }

    /**
     * 获取全局唯一uuid
     *
     * @return
     */
    public static String uid() {
        return UUID.randomUUID().toString();
    }
}

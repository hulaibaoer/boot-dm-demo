package net.longjin.bootdmdemo.utils;

import lombok.extern.slf4j.Slf4j;
import net.longjin.bootdmdemo.core.BizException;
import net.longjin.bootdmdemo.core.ResBizCode;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ：HuChao
 * @date ：Created in 2020/11/12 16:16
 * @description：通用工具类
 * @modified By：
 */
@Slf4j
public class CommonUtil {
    private static final String DEFAULT_REGEX = ",";

    /**
     * 数据集合，拼接字符串
     *
     * @param strList
     * @return
     */
    public static String jointStr(Collection<String> strList, String regex) {
        if (strList == null) return "";
        if (StringUtils.isBlank(regex)) regex = DEFAULT_REGEX;
        return StringUtils.join(strList,regex);
    }

    /**
     * 字符串转字符串集合
     *
     * @param str
     * @return
     */
    public static List<String> splitStr(String str) {
        return splitStr(str, DEFAULT_REGEX);
    }

    /**
     * 字符串转字符串集合
     *
     * @param str
     * @return
     */
    public static List<String> splitStr(String str, String regex) {
        if (StringUtils.isBlank(str)) return null;
        if (StringUtils.isBlank(regex)) regex = DEFAULT_REGEX;
        return Arrays.asList(str.split(regex)).stream().collect(Collectors.toList());
    }

}

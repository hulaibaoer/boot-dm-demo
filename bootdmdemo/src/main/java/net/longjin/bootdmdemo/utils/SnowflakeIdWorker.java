package net.longjin.bootdmdemo.utils;

/**
 * @author ：HuChao
 * @date ：Created in 2020/9/30 9:36
 * @description：雪花算法生成全局唯一自增id 0 - 0000000000 0000000000 0000000000 0000000000 0 - 00000 - 00000 - 000000000000
 * 1位标识 41位时间截(毫秒级)不是存储当前时间的时间截，而是存储时间截的差值（当前时间截 - 开始时间截) 10位的数据机器位，可以部署在1024个节点
 * 12位序列，毫秒内的计数，12位的计数顺序号支持每个节点每毫秒(同一机器，同一时间截)产生4096个ID序号
 * 整体上按照时间自增排序，并且整个分布式系统内不会产生ID碰撞(由数据中心ID和机器ID作区分)，并且效率较高，经测试，SnowFlake每秒能够产生26万ID左右
 * @modified By：
 */
public class SnowflakeIdWorker {

    /**
     * 无数据中心参数概念的单例对象，数据中心使用默认值
     */
    private static volatile SnowflakeIdWorker snowflakeIdWorker;
    /**
     * 有数据中心参数概念的单例对象
     */
    private static volatile SnowflakeIdWorker snowflakeIdWorker2;

    /**
     * 开始时间截 (2020-01-01)
     */
    private final long twepoch = 1577808000000L;

    /**
     * 机器id所占的位数
     */
    private final long workerIdBits = 5L;

    /**
     * 数据标识id所占的位数
     */
    private final long datacenterIdBits = 5L;

    /**
     * 支持的最大机器id是1023，结果是31 (这个移位算法可以很快的计算出几位二进制数所能表示的最大十进制数)
     */
    private final long maxWorkerId = -1L ^ (-1L << workerIdBits);

    /**
     * 支持的最大数据标识id，结果是31
     */
    private final long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);

    /**
     * 序列在id中占的位数
     */
    private final long sequenceBits = 12L;

    /**
     * 机器ID向左移12位
     */
    private final long workerIdShift = sequenceBits;

    /**
     * 数据标识id向左移17位(12+5)
     */
    private final long datacenterIdShift = sequenceBits + workerIdBits;

    /**
     * 时间截向左移22位(5+5+12)
     */
    private final long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;

    /**
     * 生成序列的掩码，这里为4095 (0b111111111111=0xfff=4095)
     */
    private final long sequenceMask = -1L ^ (-1L << sequenceBits);

    /**
     * 工作机器ID(0~31)
     */
    private long workerId;

    /**
     * 数据中心ID(0~31)
     */
    private long datacenterId = 1L;

    /**
     * 毫秒内序列(0~4095)
     */
    private long sequence = 0L;

    /**
     * 上次生成ID的时间截
     */
    private long lastTimestamp = -1L;

    /**
     * 构造方法
     *
     * @param workerId     机器id
     * @param datacenterId 数据中心id
     */
    private SnowflakeIdWorker(long workerId, long datacenterId) {
        if (workerId > maxWorkerId || workerId < 0) {
            throw new IllegalArgumentException(String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
        }
        if (datacenterId > maxDatacenterId || datacenterId < 0) {
            throw new IllegalArgumentException(String.format("datacenter Id can't be greater than %d or less than 0", maxDatacenterId));
        }
        this.workerId = workerId;
        this.datacenterId = datacenterId;
    }

    /**
     * 构造方法
     *
     * @param workerId 机器id
     */
    private SnowflakeIdWorker(long workerId) {
        if (workerId > maxWorkerId || workerId < 0) {
            throw new IllegalArgumentException(String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
        }
        this.workerId = workerId;
    }

    public static SnowflakeIdWorker getInstance(long workerId, long datacenterId) {
        if (snowflakeIdWorker2 == null) {
            synchronized (SnowflakeIdWorker.class) {
                if (snowflakeIdWorker2 == null) {
                    snowflakeIdWorker2 = new SnowflakeIdWorker(workerId, datacenterId);
                }
            }
        }
        return snowflakeIdWorker2;
    }

    public static SnowflakeIdWorker getInstance(long workerId) {
        if (snowflakeIdWorker == null) {
            synchronized (SnowflakeIdWorker.class) {
                if (snowflakeIdWorker == null) {
                    snowflakeIdWorker = new SnowflakeIdWorker(workerId);
                }
            }
        }
        return snowflakeIdWorker;
    }

    /**
     * 获得下一个ID (该方法是线程安全的)
     *
     * @return
     */
    public synchronized long nextId() {
        long timestamp = timeGen();
        //如果当前时间小于上一次ID生成的时间戳，说明系统时钟回退过这个时候应当抛出异常
        if (timestamp < lastTimestamp) {
            throw new RuntimeException(
                    String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
        }
        //如果是同一时间生成的，则进行毫秒内序列
        if (lastTimestamp == timestamp) {
            sequence = (sequence + 1) & sequenceMask;
            //毫秒内序列溢出
            if (sequence == 0) {
                //阻塞到下一个毫秒,获得新的时间戳
                timestamp = tilNextMillis(lastTimestamp);
            }
        } else {
            //时间戳改变，毫秒内序列重置
            sequence = 0L;
        }
        //上次生成ID的时间截
        lastTimestamp = timestamp;
        //移位并通过或运算拼到一起组成64位的ID
        return ((timestamp - twepoch) << timestampLeftShift)
                | (datacenterId << datacenterIdShift)
                | (workerId << workerIdShift)
                | sequence;
    }

    /**
     * 阻塞到下一个毫秒，直到获得新的时间戳
     *
     * @param lastTimestamp
     * @return
     */
    private long tilNextMillis(long lastTimestamp) {
        long timestamp = timeGen();
        while (timestamp <= lastTimestamp) {
            timestamp = timeGen();
        }
        return timestamp;
    }

    /**
     * 返回以毫秒为单位的当前时间
     *
     * @return
     */
    private long timeGen() {
        return System.currentTimeMillis();
    }


    /**
     * 多线程测试
     * @param args
     */
//    public static void main(String[] args) {
//        Vector<Long> ls = new Vector<>();
//        SnowflakeIdWorker snowflakeIdWorker = SnowflakeIdWorker.getInstance(1L);
//        try {
//            for(int i=0;i<10;i++){
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        long st = System.currentTimeMillis();
//                        for(int j = 0;j<100000;j++){
//                            ls.add(snowflakeIdWorker.nextId());
//                        }
//                        long ed = System.currentTimeMillis();
//                        System.err.println("thread: "+Thread.currentThread().getName()+"耗时："+(ed-st));
//                    }
//                }).start();
//            }
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }
//        try {
//            //等待3秒钟，等待异步线程结束
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println(ls.size());
//        System.out.println("list -> set");
//        Set<Long> ll = new HashSet<>(ls);
//        System.out.println(ll.size());
//    }

}

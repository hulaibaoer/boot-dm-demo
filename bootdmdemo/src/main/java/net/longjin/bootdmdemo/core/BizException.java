package net.longjin.bootdmdemo.core;

import lombok.Data;

/**
 * @author ：HuChao
 * @date ：Created in 2020/11/12 16:10
 * @description：业务相关运行时异常
 * @modified By：
 */
@Data
public class BizException extends RuntimeException {
    static final long serialVersionUID = 1L;
    /**
     * 错误码
     */
    protected int errorCode = 500;
    /**
     * 错误信息
     */
    protected String errorMsg;

    public BizException() {
        super();
    }

    public BizException(String errorMsg) {
        super(errorMsg);
        this.errorMsg = errorMsg;
    }

    public BizException(ResBizCode resBizCode) {
        this.errorCode = resBizCode.getCode();
        this.errorMsg = resBizCode.getMsg();
    }

    public BizException(ResBizCode resBizCode, String arg) {
        this.errorCode = resBizCode.getCode();
        this.errorMsg = resBizCode.getMsg() + arg;
    }
}

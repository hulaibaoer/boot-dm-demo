package net.longjin.bootdmdemo.core;

/**
 * @author ：HuChao
 * @date ：Created in 2020/11/12 16:08
 * @description：业务状态码枚举类
 * @modified By：
 */
public enum ResBizCode {
    SUCCESS(200, "成功"),
    INTERNAL_SERVER_ERROR(500, "服务器内部错误"),
    EMPTY_PARAMETER_ERROR(601, "参数不能为空"),
    REMOTE_ACCESS_FAIL(602, "远程调用接口失败"),

    USER_NOT_EXITS(1001, "用户信息不存在"),
    USER_LOGIN_ERROR(1002, "用户登录发生异常"),
    USER_AUTHENTICATION_ERROR(1003, "用户认证失败"),
    USER_UNLOGIN(1004, "用户未登录"),


    THIRD_PARTY_CASE_ARGS_ERROR(3001, "派遣单参数有误"),
    ERROR_DATE_PARSE(3002, "日期格式错误"),
    ACCEPT_THIRD_TASK_ERROR(3003, "处理接收派遣单失败！"),

    OBJ_NOT_EXIST(4001,"操作对象不存在"),
    OBJ_EXIST(4002,"操作对象已存在");


    /**
     * 代码
     */
    private int code;
    /**
     * 提示信息
     */
    private String msg;

    ResBizCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}

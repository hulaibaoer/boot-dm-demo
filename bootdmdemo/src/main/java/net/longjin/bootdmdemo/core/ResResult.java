package net.longjin.bootdmdemo.core;

import lombok.Setter;

/**
 * @author ：HuChao
 * @date ：Created in 2020/10/26 16:40
 * @description：统一结果返回实体
 * @modified By：
 */

@Setter
public class ResResult {
    /**
     * 响应代码
     */
    private int code;

    /**
     * 响应消息
     */
    private String message;

    /**
     * 响应结果
     */
    private Object result;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public ResResult() {
    }

    /**
     * 成功
     *
     * @return
     */
    public static ResResult success() {
        return success("操作成功");
    }

    /**
     * 成功
     *
     * @param data
     * @return
     */
    public static ResResult success(Object data) {
        ResResult rr = new ResResult();
        rr.setCode(ResBizCode.SUCCESS.getCode());
        rr.setMessage(ResBizCode.SUCCESS.getMsg());
        rr.setResult(data);
        return rr;
    }

    public static ResResult success(String msg) {
        ResResult rr = new ResResult();
        rr.setCode(ResBizCode.SUCCESS.getCode());
        rr.setMessage(msg);
        return rr;
    }

    /**
     * error
     *
     * @param message
     * @return
     */
    public static ResResult error(String message) {
        ResResult rr = new ResResult();
        rr.setCode(ResBizCode.INTERNAL_SERVER_ERROR.getCode());
        rr.setMessage(message);
        rr.setResult(null);
        return rr;
    }

    public static ResResult error(int code, String message) {
        ResResult rr = new ResResult();
        rr.setCode(code);
        rr.setMessage(message);
        rr.setResult(null);
        return rr;
    }

    public static ResResult error(ResBizCode resBizCode) {
        ResResult rr = new ResResult();
        rr.setCode(resBizCode.getCode());
        rr.setMessage(resBizCode.getMsg());
        rr.setResult(null);
        return rr;
    }


    public static ResResult error(ResBizCode resBizCode, Object obj) {
        ResResult rr = new ResResult();
        rr.setCode(resBizCode.getCode());
        rr.setMessage(resBizCode.getMsg());
        rr.setResult(obj);
        return rr;
    }

    public static ResResult error(ResBizCode resBizCode, String message) {
        ResResult rr = new ResResult();
        rr.setCode(resBizCode.getCode());
        rr.setMessage(resBizCode.getMsg() + message);
        rr.setResult(null);
        return rr;
    }
}
